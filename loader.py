import json
import requests


# API_KEY = 'welcome'
# API_URL = 'http://api.startupslike.me'

API_KEY = 'test'
API_URL = 'http://localhost:9292'


if __name__ == "__main__":
    with open('data.json') as data_file:
        startups = json.load(data_file)

    for data in startups:
        if data.get('error'):
            continue
        r = requests.post(
            url=str(API_URL + '/v1/startups'),
            json={
              'name': data['name'],
              'website_url': data['website_url'],
              'image_url': data['image_url'],
              'description': data['description'],
              'total_funding_raised': data['total_funding_raised'],
              'locations': data['location'],
              'industries': data['industry'],
              'number_of_employees': data['number_of_employees'].split('-')[1] if data['number_of_employees'] else None,
              'angellist_url': data['angellist'],
              'crunchbase_url': data['crunchbase'],
              'api_key': API_KEY
            }
        )
        print('{}: {}'.format(data['name'], r))
