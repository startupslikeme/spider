# Spider


get that sweet sweet data from them websites.


## Setup
- `mkvirtualenv --python=/usr/bin/python3 spider`
- `pip install requirements.txt`


## Get Starups
1. crawl startups from seed-db
  - `scrapy crawl seedDB -o seedDB.json`
2. Upload to DB
  - `python loader.py`
