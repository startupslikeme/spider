import json


if __name__ == "__main__":
    seen = set()
    duplicates = list()

    with open('data.json') as data_file:
        startups = json.load(data_file)

    with open('angel.json') as data_file:
        try:
            new_startups = json.load(data_file)
        except json.JSONDecodeError:
            new_startups = []

    for s in startups:
        if s['website_url'] in seen:
            duplicates.append(s['website_url'])
        if s['website_url'] not in seen:
            seen.add(s['website_url'])

    for s in new_startups:
        if s['website_url'] in seen:
            duplicates.append(s['website_url'])
        if s['website_url'] not in seen:
            seen.add(s['website_url'])

    print("{} potential duplicates:".format(len(duplicates)))
    print(duplicates)

    if not duplicates:
        with open('data.json', mode='w', encoding='utf-8') as outfile:
            outfile.write('[\n')

            for s in new_startups:
                json.dump(s, outfile)
                outfile.write(',\n')

            last = len(startups) - 1
            for i, s in enumerate(startups):
                json.dump(s, outfile)
                if i < last:
                    outfile.write(',\n')
                else:
                    outfile.write('\n')

            outfile.write(']\n')

        with open('angel.json', mode='w', encoding='utf-8') as outfile:
            outfile.write('')
