import scrapy
import validators


class SeedDBSpider(scrapy.Spider):
    """
    Pull websites of startups from seed-db
    then parase them for that sweet sweet data

    works in 3 steps:
     1. parse accelorator table
     2. parse starups in accelorator
     3. parse startups website
    """
    name = "seedDB"
    start_urls = ['http://www.seed-db.com/accelerators',]

    def _get_logo(self, response):
        # grab the favicon
        favicon = response.xpath('//link[@rel="icon"]/@href').extract_first()
        if favicon:
            return favicon

        # look for an image with logo in the name
        logo = response.xpath('//img[contains(@src, "logo")]/@src').extract_first()
        if logo:
            return logo

        return None

    def parse_startup_website(self, response):
        startup = response.meta['startup']

        if not startup.logo:
            startup['image_url'] = response.urljoin(self._get_logo(response))

        description = response.xpath('//meta[@name=\'description\']/@content').extract_first()
        if description:
            startup['description'] = description

        return startup

    def parse_accelerator_company(self, response):
        for row in response.xpath('//table//tbody//tr'):
            name = row.xpath('td')[1].xpath('a//text()').extract_first()

            state = row.xpath('td')[0].xpath('button//text()').extract_first()
            if state == "Dead":
                print('{} is {} yo'.format(name, state))
                continue

            website_url = row.xpath('td')[2].xpath('a//text()').extract_first()
            if website_url and not website_url.startswith('http'):
                website_url = 'http://' + website_url
            if not (website_url and validators.url(website_url)):
                continue

            funding = row.xpath('td')[-1].xpath('span//text()').extract_first()
            if funding:
                funding = int(funding.replace(',', '').strip())
            else:
                funding = None


            # request = scrapy.Request(website_url, callback=self.parse_startup_website)

            angellist = row.xpath('td//a[contains(@href, "angel")]//@href').extract_first()
            # if not angellist:
            #     continue
            # request = scrapy.Request(angellist, callback=self.parse_angellist)

            crunchbase = row.xpath('td//a[contains(@href, "crunchbase")]//@href').extract_first()
            if not crunchbase:
                continue
            request = scrapy.Request(angellist, callback=self.parse_crunchbase)

            request.meta['startup'] = {
                'name': name,
                'website_url': website_url,
                'crunchbase': crunchbase,
                'angellist': angellist,
                'total_funding_raised': funding,
            }
            yield request

    def parse_crunchbase(self, response):
        startup = response.meta['startup']

        if not response.css('#profile_header_heading').xpath('a//text()').extract_first():
            print("error loading anglelisting for {}".format(startup.get('name')))



        startup.update({
            'image_url': response.css('.company-logo').xpath('img/@src').extract_first(),

            'location': response.css('.js-location_tags').xpath('a/text()').extract(),
            'industry': response.css('.js-market_tags').xpath('a/text()').extract(),
            'number_of_employees': company_size.strip() if company_size else None,
            'description': description.strip() if description else None
        })

        return startup

    def parse_angellist(self, response):
        startup = response.meta['startup']

        if not response.css('.js-startup_name').xpath('text()').extract_first():
            print("error loading anglelisting for {}".format(startup.get('name')))

        company_size = response.css('.js-company_size').xpath('text()').extract_first()
        description = response.css('.js-startup_high_concept').xpath('text()').extract_first()

        startup.update({
            'image_url': response.css('.company-logo').xpath('img/@src').extract_first(),
            'location': response.css('.js-location_tags').xpath('a/text()').extract(),
            'industry': response.css('.js-market_tags').xpath('a/text()').extract(),
            'number_of_employees': company_size.strip() if company_size else None,
            'description': description.strip() if description else None
        })

        return startup

    def parse(self, response):
        accelerators = response.xpath("//a[contains(@href, 'acceleratorid')]//@href").extract()
        for accelerator in accelerators:
            params = accelerator.split('?')[1]
            yield scrapy.Request(
                response.urljoin('/accelerators/viewall?' + params),
                callback=self.parse_accelerator_company
            )
