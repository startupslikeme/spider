import json
from urllib.parse import urljoin

import scrapy


class LinkedInSpider(scrapy.Spider):
    """
    Crawls a starups website trying to get that sweet sweet data
    """
    name = "linkedin"

    def start_requests(self):
        with open('linkedin.json') as data_file:
            data = json.load(data_file)
        for d in data:
            yield scrapy.Request(url=d['URL'], callback=self.parse)

    def _get_logo(self, response):
        # look for an image with logo in the name
        logo = response.xpath('//img[contains(@src, "logo")]/@src').extract_first()
        if logo:
            return logo

        # grab the favicon
        favicon = response.xpath('//link[@rel="icon"]/@href').extract_first()
        return favicon

    def parse(self, response):
        logo = self._get_logo(response)
        if not logo.startswith('http'):
            logo = urljoin(response.url, logo)
        return {
            'logo': logo,
            'website_url': response.url,
            'description': response.xpath('//meta[@name=\'description\']/@content').extract_first(),
        }
