import scrapy
import validators


class YCListSpider(scrapy.Spider):
    """
    Pull websites of startups from YC
    """
    name = "yclist"
    start_urls = [
        'http://yclist.com/',
    ]

    def parse(self, response):
        headers = response.xpath('//table//th/text()').extract()

        for row in response.xpath('//table//tr'):
            company = {}
            data = []
            for column in row.xpath('td'):
                link = column.xpath('a//@href').extract_first()
                if link:
                    link = scrapy.http.Request(link).url
                    data.append(link)
                else:
                    text = column.xpath('text()').extract_first()
                    if text and text.strip():
                        data.append(text.strip())

            for i in range(len(data)):
                company[headers[i]] = data[i]

            if not validators.url(company.get('URL', '')):
                print('Invalid url: {}'.format(data))
                continue
            yield company
