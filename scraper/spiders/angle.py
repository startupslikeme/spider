import json

import scrapy


class StartupSpider(scrapy.Spider):
    """
    Crawls a starups website trying to get that sweet sweet data
    """
    name = "angel"
    handle_httpstatus_list = [404, 302]
    parsed_startups = set()

    def start_requests(self):
        with open('archives/seedDB.json') as startup_file:
            startups = json.load(startup_file)
        with open('data.json') as data_file:
            data = json.load(data_file)

        for d in data:
            self.parsed_startups.add(d['angellist'])
        for s in startups:
            if not s['angellist']:
                continue
            if s['angellist'] in self.parsed_startups:
                continue
            request = scrapy.Request(url=s['angellist'], callback=self.parse)
            request.meta['startup'] = s
            print("request {} for {}".format(s['angellist'], s['name']))
            yield request


    def parse(self, response):
        startup = response.meta['startup']
        print('scraping {}'.format(startup['name']))

        if response.status == 404:
            startup['error'] = 'angellist not found'
            return startup

        if response.status == 302:
            if 'captcha' in str(response.headers['location'], 'utf-8'):
                scrapy.utils.response.open_in_browser(response)
                raise scrapy.exceptions.CloseSpider(reason='Redirected: {}'.format(str(response.headers['location'], 'utf-8')))
            else:
                return scrapy.Request(
                    response.url,
                    callback=self.parse
                )

        if not response.css('.js-startup_name').xpath('text()').extract_first():
            scrapy.exceptions.CloseSpider(
                reason='error loading anglelisting for {}'.format(startup.get('name'))
            )

        name = response.css('.js-startup_name').xpath('text()').extract_first()
        if name:
            startup['name'] = name.strip()

        company_size = response.css('.js-company_size').xpath('text()').extract_first()
        description = response.css('.js-startup_high_concept').xpath('text()').extract_first()

        startup.update({
            'image_url': response.css('.company-logo').xpath('img/@src').extract_first(),
            'location': response.css('.js-location_tags').xpath('a/text()').extract(),
            'industry': response.css('.js-market_tags').xpath('a/text()').extract(),
            'number_of_employees': company_size.strip() if company_size else None,
            'description': description.strip() if description else None
        })

        return startup
