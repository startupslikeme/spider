# -*- coding: utf-8 -*-
import scrapy


class Startup(scrapy.Item):
    name = scrapy.Field()
    description = scrapy.Field()
    crunchbase = scrapy.Field()
    angellist = scrapy.Field()
    image_url = scrapy.Field()
    total_funding_raised = scrapy.Field()
